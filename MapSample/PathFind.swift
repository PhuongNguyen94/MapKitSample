//
//  PathFind.swift
//  MapSample
//
//  Created by Administrator on 1/26/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
import MapKit

class PathFind: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var MapView : MKMapView!


    var fromLocation : CLLocation!
    var locationManager = CLLocationManager()
    var overplay : MKOverlay?
    var direction : MKDirections?
    var foundPlace : CLPlacemark?
    var geoCoder : CLGeocoder?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = .bottom
        self.geoCoder = CLGeocoder()
        self.MapView.delegate = self
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.MapView.showsUserLocation = true
        // Do any additional setup after loading the view.
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
        self.fromLocation = locations.last
        self.updateRegion(2.0)
    }
    
    //MARK : - up date Region
    func updateRegion(_ scale:CGFloat){
        let size = self.MapView.bounds.size
        let region = MKCoordinateRegionMakeWithDistance(fromLocation.coordinate, Double(size.height * scale), Double(size.width * scale))
        self.MapView.setRegion(region, animated: true)
        
    }
    //MARK: Delegate UITextField
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (overplay != nil){
            self.MapView.remove(overplay!)
        }
        lookForAddress(textField.text! as NSString)
            return true

    }
    //MARK : - Search for location
    func lookForAddress(_ addressString : NSString){
        if (addressString == ""){
            return
        }
        self.geoCoder?.geocodeAddressString(addressString as String , completionHandler: { (placeMarks, error) in
            if (error == nil){
                self.foundPlace = placeMarks?.first
                let toPlace = MKPlacemark(placemark: self.foundPlace!)
                let formplace = MKPlacemark(coordinate: self.fromLocation.coordinate, addressDictionary: nil)
                
                self.routePlace(formplace, toPlace)
            }
          
        })
    }
    //MARK :  - Get path to place
    func routePlace(_ fromPlace:MKPlacemark,_ toLocation : MKPlacemark){
        let request  = MKDirectionsRequest()
        let fromMapItem = MKMapItem(placemark: fromPlace)
        request.source = fromMapItem
        let tomapItem = MKMapItem(placemark: toLocation)
        request.destination = tomapItem
        self.direction = MKDirections(request: request)
        self.direction?.calculate(completionHandler: { (response, error) in
            if error == nil{
                self.showRoute(response!)
            }
        })
    }
    //MARK : paint road
    func showRoute(_ response : MKDirectionsResponse){
        for route in response.routes {
            self.overplay = route.polyline
            self.MapView.add(overplay!, level: .aboveRoads)
            for step in route.steps{
                print(step.instructions)
            }
        }
    }
    //MARK: - add line road
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overplay!)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 5.0
        return renderer
    }
    //MARK : - add annotation
//    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
//        let annotioneView = MapView.dequeueReusableAnnotationView(withIdentifier: "Pin")
//    }


}
